Typometer
=========

fiëé visuëlle
Henning Hraban Ramm
http://www.fiee.net

![typographical ruler with lots of scales, dedicated to Tako Hoekwater](https://codeberg.org/fiee/context-examples/raw/branch/master/typometer/typometer-taco.png "ConTeXt meeting 2022 typometer")

For the ConTeXt meeting 2022 I wanted to have a typometer (typographical ruler)
as a gift to the participants. Since a proper production in polycarbonate was
out of question (i.e. I don’t know any “makers”), I had it exposed on litho film
that is still used in some small printshops for making printing plates.
Of course my design is written in MetaFun for ConTeXt LMTX, but my MetaPost
skills are still quite lacking.

In the process, Taco showed me something similar from EuroBachoTeX 2019, and
Bogusław Jackowski (GUST) provided me with his code for it that was originally
made for EuroTeX 1994, and another by Victor Ejkhout from at least 1996.

Links
-----
ConTeXt meeting: https://meeting.contextgarden.net
